This is an Othello AI

Written by Justin Koch for CS2
3/8/2015

Group Contributions:
All work was done by myself.

AI Improvements
My AI consists of a depth 2 search that maximizes the minimum result acording to a heuristic function. The function takes into account the coin differential with a higher weight for moves that control the corners or edges and a lower weight for moves that are adjacent to corners. I started working on a more complicated heuristic function that also took into account other factors like mobility or locking positions but didn't finish it. Instead I tweaked the weights of my heuristic function to maximize winning against constantPlayer. My strategy may not search as deep as other programs but I think the heuristic function will help it perform well. 
