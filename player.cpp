#include "player.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
//small change
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;

    myBoard = new Board();

    //method = HEURISTIC;
    //method = MINIMAX_2;
    method = HYBRID;

    mySide = side;

    //define map weight function
    //sourced from https://github.com/kartikkukreja/blog-codes/blob/master/src/Heuristic%20Function%20for%20Reversi%20%28Othello%29.cpp
    //didn't use
    /*
    int V[8][8] = {
        {20, -3, 11, 8, 8, 11, -3, 20},
        {-3, -7, -4, 1, 1, -4, -7, -3},
        {11, -4, 2, 2, 2, 2, -4, 11},
        {8, 1, 2, -3, -3, 2, 1, 8},
        {8, 1, 2, -3, -3, 2, 1, 8},
        {11, -4, 2, 2, 2, 2, -4, 11},
        {-3, -7, -4, 1, 1, -4, -7, -3},
        {20, -3, 11, 8, 8, 11, -3, 20}}; */

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
  
    //Process oponenet's move
    if( opponentsMove != NULL){
        if (mySide == WHITE){
            myBoard->doMove(opponentsMove, BLACK);
        }
        else{
            myBoard->doMove(opponentsMove, WHITE);
        }
    }

    Move *toDo;


    if(method == HYBRID){
        toDo = doHybridMove();
    }
    else if(method == MINIMAX_2){
        toDo = doMinimaxTwo();
    }
    else if(method == HEURISTIC){
        toDo = doHeuristicMove();
    }
    else{
        toDo = doFirstMove();
    }


    myBoard->doMove(toDo, mySide);

    return toDo;

}


Move *Player::doFirstMove(){

    //Get available moves
    vector<Move>* myMoves = getAvailableMoves(mySide, myBoard); 
    if(myMoves->size() == 0){
        return NULL;
    }

    Move *doThis = &(myMoves->front());

    return doThis;

}

Move *Player::doHybridMove(){
    //Get available moves
    vector<Move>* Level1_Moves = getAvailableMoves(mySide, myBoard); 
    if(Level1_Moves->size() == 0){
        return NULL;
    }

    double maxMin = -9999999;
    int maxMin_x = -1;
    int maxMin_y = -1;

    //Move *doThis = &(Level1_Moves->front());

    for(vector<Move>::iterator it = Level1_Moves->begin(); it != Level1_Moves->end(); ++it){

        Board *testBoard = myBoard->copy();
        testBoard->doMove(&(*it), mySide);

        vector<Move>* Level2_Moves; 
        
        if(mySide == WHITE)
            Level2_Moves = getAvailableMoves(BLACK, testBoard); 
        else
            Level2_Moves = getAvailableMoves(WHITE, testBoard); 

        double minScore = 9999999;
        double score;

        for(vector<Move>::iterator it2 = Level2_Moves->begin(); it2 != Level2_Moves->end(); ++it2){

            Board *testBoard2 = testBoard->copy();
            if (mySide == WHITE){testBoard2->doMove(&(*it2), BLACK);}
            else{testBoard2->doMove(&(*it2), WHITE);}

            score = applyHeuristic1(mySide, testBoard2, &(*it) );
            //std::cerr << "score: " << score << std::endl;

            if(score < minScore){
                minScore = score;
            }
        }

        if (minScore > maxMin){

            maxMin = minScore;
            maxMin_x = it->x;
            maxMin_y = it->y;
            
        }

    }

    Move *doThis = new Move(maxMin_x, maxMin_y);


    return doThis;

}

Move *Player::doMinimaxTwo(){
    //Get available moves
    vector<Move>* Level1_Moves = getAvailableMoves(mySide, myBoard); 
    if(Level1_Moves->size() == 0){
        return NULL;
    }

    double maxMin = -9999999;
    int maxMin_x = -1;
    int maxMin_y = -1;

    //Move *doThis = &(Level1_Moves->front());

    for(vector<Move>::iterator it = Level1_Moves->begin(); it != Level1_Moves->end(); ++it){

        Board *testBoard = myBoard->copy();
        testBoard->doMove(&(*it), mySide);

        vector<Move>* Level2_Moves; 
        
        if(mySide == WHITE)
            Level2_Moves = getAvailableMoves(BLACK, testBoard); 
        else
            Level2_Moves = getAvailableMoves(WHITE, testBoard); 

        double minScore = 9999999;
        double score;

        for(vector<Move>::iterator it2 = Level2_Moves->begin(); it2 != Level2_Moves->end(); ++it2){

            Board *testBoard2 = testBoard->copy();
            if (mySide == WHITE){testBoard2->doMove(&(*it2), BLACK);}
            else{testBoard2->doMove(&(*it2), WHITE);}

            if(mySide == WHITE){
                score = testBoard2->countWhite() - testBoard2->countBlack();
            }
            else{
                score = testBoard2->countBlack() - testBoard2->countWhite();
            }

            if(score < minScore){
                minScore = score;
            }
        }

        if (minScore > maxMin){

            maxMin = minScore;
            maxMin_x = it->x;
            maxMin_y = it->y;
            
        }

    }

    Move *doThis = new Move(maxMin_x, maxMin_y);


    return doThis;
}

Move *Player::doHeuristicMove(){

    //Get available moves
    vector<Move>* myMoves = getAvailableMoves(mySide, myBoard); 
    if(myMoves->size() == 0){
        return NULL;
    }

    Move *doThis = &(myMoves->front());
    double currScore = -9999;

    for(vector<Move>::iterator it = myMoves->begin(); it != myMoves->end(); ++it){
        double score;
        Board *testBoard = myBoard->copy();
        testBoard->doMove(&(*it), mySide);

        if(mySide == WHITE){
            score = testBoard->countWhite() - testBoard->countBlack();
        }
        else{
            score = testBoard->countBlack() - testBoard->countWhite();
        }
        

        //corner pieces
        if (  (it->x == 0 || it->x == 7) && (it->y == 0 || it->y == 7) ){
            score = score * 5;
        }

        

        //Next to corner pieces
        if (  (it->x == 1 || it->x == 6) && (it->y == 1 || it->y == 6) ){
            score = score * .2;
        }
        if (  (it->x == 1 || it->x == 6) && (it->y == 0 || it->y == 7) ){
            score = score * .2;
        }
        if (  (it->x == 0 || it->x == 7) && (it->y == 1 || it->y == 6) ){
            score = score * .2;
        } 

        //Edges
        if (  (it->x == 0) && (it->y >= 2 && it->y <= 5) ){
            score = score * 2;
        }
        if (  (it->x == 7) && (it->y >= 2 && it->y <= 5) ){
            score = score * 2;
        }
        if (  (it->x >= 2 && it->x <= 5) && (it->y == 0) ){
            score = score * 2;
        }
        if (  (it->x >= 2 && it->x <= 5) && (it->y == 7) ){
            score = score * 2;
        }




        if (score > currScore){
            currScore = score;
            doThis->x = it->x;
            doThis->y = it->y;
        }


    }

    //doThis = &(myMoves->front());


    return doThis;
}

double Player::applyHeuristic1(Side side, Board *board, Move *move){
    double score = 0;
    double factor = 0;

    //calculate score
    if(side == WHITE){
        score = board->countWhite() - board->countBlack();
    }
    else{
        score = board->countBlack() - board->countWhite();
    }

    //corner pieces
    if (  (move->x == 0 || move->x == 7) && (move->y == 0 || move->y == 7) ){
        factor = factor + 5;
    }

    

    //Next to corner pieces
    if (  (move->x == 1 || move->x == 6) && (move->y == 1 || move->y == 6) ){
        factor = factor + .2;
    }
    if (  (move->x == 1 || move->x == 6) && (move->y == 0 || move->y == 7) ){
        factor = factor + .2;
    }
    if (  (move->x == 0 || move->x == 7) && (move->y == 1 || move->y == 6) ){
        factor = factor + .2;
    } 

    //Edges
    if (  (move->x == 0) && (move->y >= 2 && move->y <= 5) ){
        factor = factor + 2;
    }
    if (  (move->x == 7) && (move->y >= 2 && move->y <= 5) ){
        factor = factor + 2;
    }
    if (  (move->x >= 2 && move->x <= 5) && (move->y == 0) ){
        factor = factor + 2;
    }
    if (  (move->x >= 2 && move->x <= 5) && (move->y == 7) ){
        factor = factor + 2;
    }



    //apply factor
    if(score > 0){
        score = score * factor;
    }
    else if(factor != 0){
        score = score / factor;
    }

    return score;
}

double Player::applyHeuristic2(Side side, Board *board, Move *move){
    double score = 0;
    //double ownership = 0;
    //double corners = 0;
    //double cornerCloseness = 0;
    //double mobility = 0;
    double mapScore = 0;
    double factor = 0;

    //iterate through board and score
    for (int x = 0; x < 8; x++){
        for (int y = 0; y < 8; y++){
            if (board->occupied(x, y)){
                if (board->get(side, x, y)){
                    //mapScore += V[x][y];
                }
                else{
                    //mapScore -= V[x][y];
                }

            }
        }
    }

    //mobility


    //calculate score
    if(side == WHITE){
        score = board->countWhite() - board->countBlack();
    }
    else{
        score = board->countBlack() - board->countWhite();
    }

    //calculate corner factor

    //corner pieces
    if (  (move->x == 0 || move->x == 7) && (move->y == 0 || move->y == 7) ){
        factor = factor + 10;
    }

    

    //Next to corner pieces
    if (  (move->x == 1 || move->x == 6) && (move->y == 1 || move->y == 6) ){
        factor = factor + .1;
    }
    if (  (move->x == 1 || move->x == 6) && (move->y == 0 || move->y == 7) ){
        factor = factor + .1;
    }
    if (  (move->x == 0 || move->x == 7) && (move->y == 1 || move->y == 6) ){
        factor = factor + .1;
    } 

    //Edges
    if (  (move->x == 0) && (move->y >= 2 && move->y <= 5) ){
        factor = factor + 4;
    }
    if (  (move->x == 7) && (move->y >= 2 && move->y <= 5) ){
        factor = factor + 4;
    }
    if (  (move->x >= 2 && move->x <= 5) && (move->y == 0) ){
        factor = factor + 4;
    }
    if (  (move->x >= 2 && move->x <= 5) && (move->y == 7) ){
        factor = factor + 4;
    }



    //apply factor
    if(score > 0){
        score = score * factor;
    }
    else if(factor != 0){
        score = score / factor;
    }

    return score + mapScore;
}

vector<Move>* Player::getAvailableMoves(Side side, Board *board){

    vector<Move>* availableMoves = new vector<Move>;

    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {

            Move move(i, j);

            if (board->checkMove(&move, side)){
                availableMoves->push_back(move);
            }
        }
    }

    return availableMoves;

}