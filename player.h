#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include <vector>

#define FIRST 0
#define HEURISTIC 1
#define MINIMAX_2 2
#define HYBRID 3

using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board *myBoard;
    Side mySide;
    int method;
    int V[8][8];

private:
	Move *doFirstMove();
	Move *doHeuristicMove();
    Move *doMinimaxTwo();
    Move *doHybridMove();

	vector<Move>* getAvailableMoves(Side side, Board *board);
    double applyHeuristic1(Side side, Board *board, Move *move);
    double applyHeuristic2(Side side, Board *board, Move *move);
};

#endif
